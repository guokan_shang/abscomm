## Abstractive Community Detection
This repo contains the source code for the following paper:

[Energy-based Self-attentive Learning of Abstractive Communities for Spoken Language Understanding](https://arxiv.org/abs/1904.09491)

1.Git clone [AMI-Corpus-Annotations](https://bitbucket.org/guokan_shang/ami-corpus-annotations) repository under `data/`, follow instructions there and generate *dialogueActs* and *summlink* annotations.

```
git clone https://guokan_shang@bitbucket.org/guokan_shang/ami-corpus-annotations.git
```

2.Run `core.py` with python3.
```
ipython core.py
```